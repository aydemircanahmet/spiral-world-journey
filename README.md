Spiral World Journey Privacy Policy 
<ENGLISH>
We don't collect any personal data. 
All game progress data is stored local. 
Our advertisement has no target age group. 
So we don't recommend to play it if you are under 13. 
In case you want to share your score, game just tooks a screenshot. It's all up to you share or not.
</ENGLISH>

<TURKISH>
Herhangi bir kişisel veri toplamıyoruz. 
Oyun içi ilerleme bilgisinin kaydı sadece cihazınızda tutulmaktadır. 
Reklamlarımızın bir hedef yaş kitlesi yoktur. Eğer 13 yaş altıysanız, oynamanızı önermiyoruz. 
Eğer oyun içi puanınızı paylaşmak isterseniz, oyun sadece ekran görüntüsü alır. Paylaşıp paylaşmamak sizin kararınızdır.
</TURKISH>